Overcite is a simple Python script which uses regular expressions to extract in-text citations from academic papers.

Overcite can match the following types of citations, in both inline, such as Foo (2013), and isolated, such as (Foo, 2013) forms:
* Foo (2013)
* Foo and Bar (2013, p.123)
* Foo, Bar, & Baz (2013)
* Qux et al. (2013)
* Foo and Bar, Baz, & Qux, et al. (2013)

There is still some more work to do, as detailed in the open issues, and please feel free to request more!