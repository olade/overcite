from nose import with_setup
from nose.tools import raises

from overcite import extract

## -----------------
## True positives
## -----------------

def testNameYear():
    text = "A quote (Foo, 2013). More text."
    assert extract(text) == ["Foo, 2013"]

def testNameYearInline():
    text = "A quote from Foo (2013), followed by more text."
    assert extract(text) == ["Foo (2013)"]

def testNameApostropheInline():
    text = "A quote from Foo's (2013) study."
    assert extract(text) == ["Foo's (2013)"]

def testApostropheName():
    text = "A quote from O'Foo and Bar'tok (2013) with more text."
    assert extract(text) == ["O'Foo and Bar'tok (2013)"]

def testNamePage():
    text = "A quote (Foo, 2013, p. 123) with more text."
    assert extract(text) == ["Foo, 2013, p. 123"]

def testNamePageInline():
    text = "A quote from Foo (2013, p. 123) with more text."
    assert extract(text) == ["Foo (2013, p. 123)"]

def testNameApostrophePageInline():
    text = "A quote from Foo's (2013) study (p. 123)."
    assert extract(text) == ["Foo's (2013)"]

def testNameApostrophePageCombinedInline():
    text = "A quote from Foo's (2013, p. 123) study."
    assert extract(text) == ["Foo's (2013, p. 123)"]


def testNameEtAl():
    text = "A quote (Foo et al, 2013). More text."
    print text + ": (" + str(len(extract(text))) + ") " + "\n".join(extract(text))
    assert extract(text) == ["Foo et al, 2013"]
    text = "A quote (Foo et al., 2013). More text."
    print text + ": (" + str(len(extract(text))) + ") " + "\n".join(extract(text))
    assert extract(text) == ["Foo et al., 2013"]
    text = "A quote (Foo, et al, 2013). More text."
    print text + ": (" + str(len(extract(text))) + ") " + "\n".join(extract(text))
    assert extract(text) == ["Foo, et al, 2013"]
    text = "A quote (Foo, et al., 2013). More text."
    print text + ": (" + str(len(extract(text))) + ") " + "\n".join(extract(text))
    assert extract(text) == ["Foo, et al., 2013"]

def testNameEtAlInline():
    text = "A quote from Foo et al (2013), followed by more text."
    print text + ": (" + str(len(extract(text))) + ") " + "\n".join(extract(text))
    assert extract(text) == ["Foo et al (2013)"]
    text = "A quote from Foo et al. (2013), followed by more text."
    print text + ": (" + str(len(extract(text))) + ") " + "\n".join(extract(text))
    assert extract(text) == ["Foo et al. (2013)"]
    text = "A quote from Foo, et al. (2013), followed by more text."
    print text + ": (" + str(len(extract(text))) + ") " + "\n".join(extract(text))
    assert extract(text) == ["Foo, et al. (2013)"]
    text = "A quote from Foo, et al. (2013), followed by more text."
    print text + ": (" + str(len(extract(text))) + ") " + "\n".join(extract(text))
    assert extract(text) == ["Foo, et al. (2013)"]


def testAndNames():
    text = "A quote (Foo and Bar, 2013). More text."
    assert extract(text) == ["Foo and Bar, 2013"]
    text = "A quote (Foo & Bar, 2013). More text."
    assert extract(text) == ["Foo & Bar, 2013"]

def testAndNamesInline():
    text = "A quote from Foo and Bar (2013), followed by more text."
    assert extract(text) == ["Foo and Bar (2013)"]
    text = "A quote from Foo & Bar (2013), followed by more text."
    assert extract(text) == ["Foo & Bar (2013)"]

def testCommaNames():
    text = "A quote (Foo, Bar, 2013). More text."
    assert extract(text) == ["Foo, Bar, 2013"]

def testCommaNamesInline():
    text = "A quote from Foo, Bar (2013), followed by more text."
    assert extract(text) == ["Foo, Bar (2013)"]

def testExtraAnd():
    text = "A quote from and Foo and Bar (2013), followed by more text."
    assert extract(text) == ["Foo and Bar (2013)"]
    text = "A quote from and Foo and Bar (2013) and, followed by more text."
    assert extract(text) == ["Foo and Bar (2013)"]


def testAndNamesEtAl():
    text = "A quote (Foo and Bar et al, 2013). More text."
    assert extract(text) == ["Foo and Bar et al, 2013"]
    text = "A quote (Foo and Bar et al., 2013). More text."
    assert extract(text) == ["Foo and Bar et al., 2013"]
    text = "A quote (Foo and Bar, et al, 2013). More text."
    assert extract(text) == ["Foo and Bar, et al, 2013"]
    text = "A quote (Foo and Bar, et al., 2013). More text."
    assert extract(text) == ["Foo and Bar, et al., 2013"]

def testAndNamesInlineEtAl():
    text = "A quote from Foo and Bar et al (2013), followed by more text."
    assert extract(text) == ["Foo and Bar et al (2013)"]
    text = "A quote from Foo and Bar et al. (2013), followed by more text."
    assert extract(text) == ["Foo and Bar et al. (2013)"]
    text = "A quote from Foo and Bar, et al (2013), followed by more text."
    assert extract(text) == ["Foo and Bar, et al (2013)"]
    text = "A quote from Foo and Bar, et al. (2013), followed by more text."
    assert extract(text) == ["Foo and Bar, et al. (2013)"]

def testCommaNamesEtAl():
    text = "A quote (Foo, Bar et al, 2013). More text."
    assert extract(text) == ["Foo, Bar et al, 2013"]
    text = "A quote (Foo, Bar et al., 2013). More text."
    assert extract(text) == ["Foo, Bar et al., 2013"]
    text = "A quote (Foo, Bar, et al, 2013). More text."
    assert extract(text) == ["Foo, Bar, et al, 2013"]
    text = "A quote (Foo, Bar, et al., 2013). More text."
    assert extract(text) == ["Foo, Bar, et al., 2013"]

def testCommaNamesInlineEtAl():
    text = "A quote from Foo, Bar et al (2013), followed by more text."
    assert extract(text) == ["Foo, Bar et al (2013)"]
    text = "A quote from Foo, Bar et al. (2013), followed by more text."
    assert extract(text) == ["Foo, Bar et al. (2013)"]
    text = "A quote from Foo, Bar, et al (2013), followed by more text."
    assert extract(text) == ["Foo, Bar, et al (2013)"]
    text = "A quote from Foo, Bar, et al. (2013), followed by more text."
    assert extract(text) == ["Foo, Bar, et al. (2013)"]


def testMixed():
    text = "A quote from Foo and Bar, Baz, & Qux, et al. (2013) with more text."
    assert extract(text) == ["Foo and Bar, Baz, & Qux, et al. (2013)"]


def testDupes():
    text = "A quote from Foo (2013) and again with the Foo (2013)."
    assert extract(text) == ["Foo (2013)", "Foo (2013)"]

def testDedupe():
    text = "A quote from Foo (2013) and again with the Foo (2013)."
    assert extract(text, dedupe=True) == ["Foo (2013)"]


## ----------------------------
## Potential false positives
## ----------------------------

def testNoYear():
    assert(extract("Foo and Bar") == [])
    assert(extract("Foo ()") == [])
    assert(extract("Foo (213)") == [])

def testNoYearComma():
    assert(extract("Foo 2013") == [])
    assert(extract("(Foo 2013)") == [])

def testIllegalAnd():
    assert(extract("Foo and (2013)") == [])
    assert(extract("(Foo and 2013)") == [])
